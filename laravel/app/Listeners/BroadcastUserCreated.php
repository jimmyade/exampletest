<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Auth\Events\Registered;
use App\Events\UserCreated;

class BroadcastUserCreated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
 
    public function __construct()
    {
     
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(Registered $event)
    {
        broadcast(new UserCreated("{$event->user->name} is registered"));
    }
}
