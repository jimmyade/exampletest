<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Events\UserCreated;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\User;


class UserCreationTest extends TestCase
{
    //use DatabaseTransactions;
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    private function validParams($overrides = [])
    {
        return array_merge([
            'name' => 'JJimmy 11111',
            'email' => 'jjja11@gmail.com',
            'password' => '7kkkogaddd@@',
        ], $overrides);
    }

    /** @test */
    function an_event_when_a_user_is_added()
    {

       //fake event dispatcher
        Event::fake([UserCreated::class]);
      
      //  $user = User::factory()->create();
        //Create a user with Json request
        $response = $this->json('POST', 'api/users', $this->validParams())
         ->assertSuccessful()
         ->assertJsonStructure([
            "user" => [
                'id',
                'name',
                'email',
                'created_at',
                'updated_at',
            ],   
            "message"
        ]);
     
        //check if created user is in the database
        $this->assertDatabaseHas('users', ['name' => 'JJimmy 11111']);

        //ASSETION TO SEE IF EVENT DISPATCHED
        Event::assertDispatched(UserCreated::class, function ($event) {
            $user1 = User::firstOrFail();
            return $event->user->is($user1);
        });
    }



        
    
}
