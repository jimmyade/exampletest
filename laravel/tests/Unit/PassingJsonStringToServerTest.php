<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use Ratchet\ConnectionInterface;
use Ratchet\WebSocket\MessageComponentInterface;
use Ratchet\Mock\Connection;
use Ratchet\Server\EchoServer;
use Ratchet\Mock\WampComponent as TestComponent;

class PassingJsonStringToServerTest extends TestCase
{
   

    public function test_sends_json_data() {
        //Test Message that will be sent
        $uri = 'http://localhost/endpoint';
        $message = array(6, $uri);
        $data=json_encode($message);
        // Create a mock for the ConnectionInterface,
        $messgaesender= $this->createMock('\Ratchet\ConnectionInterface');
        // Set up the expectation for the send() method
        // to be called only once and with the variable message
        // as its parameter.
        $messgaesender->expects($this->once())
        ->method('send')
        ->with($data);
        //creating an echo server to get back the message that was sent
         $echoserver=new EchoServer;
   
         $echoserver->onMessage($messgaesender, $data);
   
   
       }
   
    



 



}
