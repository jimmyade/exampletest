<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use Ratchet\Server\EchoServer;
use Ratchet\ConnectionInterface;
use Ratchet\RFC6455\Messaging\MessageInterface;
use Ratchet\WebSocket\MessageComponentInterface;

class CheckServerConnectivityTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_message_sent_and_recieved() {

        //Test Message that will be sent
        $data = 'Test that it connects and send message';
        // Create a mock for the ConnectionInterface,
        $messgaesender= $this->createMock('\Ratchet\ConnectionInterface');
        // Set up the expectation for the send() method
        // to be called only once and with the variable message
        // as its parameter.
        $messgaesender->expects($this->once())
        ->method('send')
        ->with($data);
        //creating an echo server to get back the message that was sent
         $echoserver=new EchoServer;

         $echoserver->onMessage($messgaesender, $data);

       
    }


    public function test_to_close_connection_if_error() {

         $echoserver=new EchoServer;
         $messgaesender= $this->createMock('\Ratchet\ConnectionInterface');
           // Set up the expectation for the close() method
          // to be called only once 
         $messgaesender->expects($this->once())->method('close');
         //throws new error on error
         $echoserver->onError($messgaesender, new \Exception);
        
    }



  
  

}
