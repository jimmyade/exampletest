FROM php:8-apache
#FROM phpmyadmin/phpmyadmin


# Copy composer.lock and composer.json
COPY composer.lock composer.json /var/www/laravel/

# Set working directory
WORKDIR /var/www/laravel
RUN apt-get update -y && apt-get install -y openssl zip unzip git
    # Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*
RUN docker-php-ext-install pdo pdo_mysql
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
# CMD php artisan serve --host=0.0.0.0 --port=8080
EXPOSE 8081
