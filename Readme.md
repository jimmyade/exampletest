## About Task

Please create a fork with this repo and share your repository via email. and make sure you commit on every time you implement or tests something ( follow micro-commit approach )

Testing Send-Receive Message using Web-Socket 

- Write a test that sends example messages to the server and receive message. 
- Write a test that sends json string and check if it's accepted by the server


Just make sure your tests can be called by using this command,

``` docker exec CONTAINER_ID sh -c "php vendor/phpunit/phpunit/phpunit" ```

Hint : https://laravel.com/docs/8.x/broadcasting


## PUSHER SETTUP

Go to pusher.com set up your account and get the credentials that you will use to setup pusher 

please make sure you change on the .env file the following parameters: 

PUSHER_APP_ID=1235567
PUSHER_APP_KEY=e1ac2962c55f39a59c20
PUSHER_APP_SECRET=d3e90ee5d138d52e5093
PUSHER_APP_CLUSTER=eu

Change the parameters to the setting on your pusher account

Also on the .env file change BROADCAST_DRIVER to pusher

BROADCAST_DRIVER=pusher







